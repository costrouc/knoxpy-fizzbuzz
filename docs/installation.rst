Installation
============

This package can be installed via ``pypi`` and ``conda``.

.. code-block:: shell

   pip install knoxpy-fizzbuzz

.. code-block:: shell

   conda install -c costrouc knoxpy-fizzbuzz
