.. knoxpy-fizzbuzz documentation master file, created by
   sphinx-quickstart on Thu Jul  5 16:07:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to knoxpy-fizzbuzz's documentation!
===========================================

This is a super advanced `fizzbuzz
<https://en.wikipedia.org/wiki/Fizz_buzz>`_ implementation!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
