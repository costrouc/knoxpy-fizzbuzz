from fizzbuzz import fizzbuzz


def test_example():
    assert 1 == 1


def test_fizzbuzz(capsys):
    fizzbuzz(9)
    captured = capsys.readouterr()
    assert captured.out == '1\n2\nFizz\n4\nBuzz\nFizz\n7\n8\nFizz\n'
